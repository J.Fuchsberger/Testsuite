# -*- mode: cmake; coding: utf-8; indent-tabs-mode: nil; -*-
# kate: syntax cmake; space-indent on; indent-width 2; encoding utf-8;
# kate: auto-brackets on; mixedindent off; indent-mode cstyle;

#-------------------------------------------------------------------------------
# Some config variables.
#-------------------------------------------------------------------------------
#SET(CFS_SOURCE_DIR "@CFS_SOURCE_DIR@")
SET(USE_OPENMP "@USE_OPENMP@")
SET(USE_PETSC "@USE_PETSC@")
SET(CFS_BINARY_DIR "@CFS_BINARY_DIR@")
SET(CFS_ARCH_STR "@CFS_ARCH_STR@")
SET(WIN32 "@WIN32@")
SET(TARGET_SYSTEM_EMULATOR "@TARGET_SYSTEM_EMULATOR@")
SET(CMAKE_EXECUTABLE_SUFFIX "@CMAKE_EXECUTABLE_SUFFIX@")
SET(PYTHON_EXECUTABLE "@PYTHON_EXECUTABLE@")
set(TESTSUITE_CFSTOOL_MODE "@TESTSUITE_CFSTOOL_MODE@")
set(TESTSUITE_THREADS "@TESTSUITE_THREADS@")

#-------------------------------------------------------------------------------
# Paths to some binaries.
#-------------------------------------------------------------------------------
SET(H5DUMP_BINARY "${CFS_BINARY_DIR}/bin/h5dump${CMAKE_EXECUTABLE_SUFFIX}")

# TEST_TYPE can be defined in the CMakeLists.txt of a test to e.g. cfsdat
# if not set, the default is cfs
IF(NOT TEST_TYPE)
  SET(TEST_TYPE "cfs")
ENDIF(NOT TEST_TYPE)
# Note that this are scripts
SET(CFS_BINARY "${CFS_BINARY_DIR}/bin/${TEST_TYPE}")

SET(CFSTOOL_BINARY "${CFS_BINARY_DIR}/bin/cfstool")

#-------------------------------------------------------------------------------
# Path to testsuite source directory.
#-------------------------------------------------------------------------------
SET(TESTSUITE_SRC_DIR "@TESTSUITE_DIR@")

#-------------------------------------------------------------------------------
# Path to testsuite output directory.
#-------------------------------------------------------------------------------
SET(TESTSUITE_BIN_DIR "@TESTSUITE_BIN_DIR@")

#-------------------------------------------------------------------------------
# Include some macros. Especially the one for generating test names out of
# directory names.
#-------------------------------------------------------------------------------
INCLUDE("${TESTSUITE_SRC_DIR}/TestMacros.cmake")

#-------------------------------------------------------------------------------
# Macro with standard CFS++ test routine.
# if TEST_INFO_XML is set, compares *.info.xml against *.ref.info.xml via compare_info_xml.py for defined cases! (see e.g. BlochMode2D)
# else compare results_hdf5/*.h5 against *.h5ref (standard!!)
#-------------------------------------------------------------------------------
MACRO(CFS_STANDARD_TEST)
  # Generate test names
  GENERATE_TEST_NAME_AND_FILE("${CURRENT_TEST_DIR}")

  # Determine which files need to be copied to TESTSUITE_BIN_DIR
  DETERMINE_TEST_FILE_LIST()

  # Delete previously generated files.
  CLEANUP_TEST_DIR()

  # Copy files over to TESTSUITE_BIN_DIR in order to avoid running concurrent
  # tests in TESTSUITE_SRC_DIR, which might mess up test results. Another reason
  # is, that the source tree does not get cluttered with the output of simulations
  # any more. This makes the use of SVN or Git much more convenient.
  COPY_TEST_FILES("${TEST_FILE_LIST}" "${SKIP_H5REF}")
  
  # Check number of nodes < 1000 by CHECK_NUMBER_OF_NODES_AND_ELEMS removed from Fabian 22.1.2016

  # add -t option for cfs-tests
  IF(TEST_TYPE STREQUAL "cfs")
    IF(TESTSUITE_THREADS LESS 2 AND USE_OPENMP)
      MESSAGE("You are using USE_OPENMP but test only with 1 thread. This might lead to un-discovered multithreading bugs!")
    ENDIF(TESTSUITE_THREADS LESS 2 AND USE_OPENMP)
    SET(CFS_ARGS ${CFS_ARGS} "-t${TESTSUITE_THREADS}")
  ENDIF(TEST_TYPE STREQUAL "cfs")

  # Use default mode if not specified
  if("${CFSTOOL_MODE}" STREQUAL "")
    message("Default CFSTOOL_MODE is set.")
    set(CFSTOOL_MODE ${TESTSUITE_CFSTOOL_MODE})
  else()
    message("Local CFSTOOL_MODE is set.")
  endif()  
  message("Used CFSTOOL_MODE: ${CFSTOOL_MODE}")

  # Run CFS++ on the simulation
  IF(USE_PETSC)
    RUN_MPI_TEST_SIMULATION()
  ELSE()
    RUN_TEST_SIMULATION()
  ENDIF()
  IF(TEST_INFO_XML) # set in the testcase via -DTEST_INFO_XML:STRING="ON"
    DIFF_TEST_RESULTS_INFO_XML("${EPSILON}" "${SKIP_NOISE}" "${LAST}")
  elseif(TEST_HIST_FILE)
    DIFF_TEST_RESULTS_HIST_FILE("${EPSILON}")
  else()
    # Run cfstool to diff against the .h5ref.
    DIFF_TEST_RESULTS_CFSTOOL("${EPSILON}" "${CFSTOOL_MODE}")
  ENDIF()
  
ENDMACRO(CFS_STANDARD_TEST)

CFS_STANDARD_TEST()
