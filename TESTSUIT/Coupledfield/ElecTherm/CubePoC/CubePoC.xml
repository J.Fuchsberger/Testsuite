<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">

   <documentation>
        <title> ElecHeat coupled simulation</title>
        <authors>
            <author>Manfred Kaltenbacher and Helmut Koeck</author>
        </authors>
        <date>2014-01-16</date>
        <keywords>
            <keyword>elecConduction - heat</keyword>
        </keywords>
        <references>n.a. </references>
        <isVerified>no</isVerified>
        <description>
            This example served as a test case while implementing
            the heat conduction PDE. The model consists of 3 identical blocks (3D) with
            the source at the bottom, a possible non-linear layer inbetween (IMD)
            and a top layer (PM). All regions are meshed with brick elements (linear).
            The analysis type now is TRANSIENT.
        </description>
    </documentation>
       
    <fileFormats>
        <input>
            <hdf5 fileName="CubePoC_conform_v1.h5" id="smodel"/> 
        </input>
        
        <output>
            <hdf5 id="hdf5"/>
            <text id="txt"/>
        </output>
        <materialData file="IRmat_NL.xml"/>
    </fileFormats>
    
    <domain geometryType="3d">
        <regionList>
            <region name="Vol_EPI" material="Si8res"/>
            <region name="Vol_IMD" material="Si8res"/>
            <region name="Vol_PM" material="Cu1"/>
        </regionList>

        <nodeList>
            <nodes name="bc_top_manual">
                <list>
                    <freeCoord comp="x" start="0.0e-3" stop="1.00e-3" inc="0.1e-3"/>
                    <freeCoord comp="z" start="0.0e-3" stop="1.00e-3" inc="0.1e-3"/>
                    <fixedCoord comp="y" value="3.0e-3"/>
                </list>
            </nodes>        
            <nodes name="bc_bottom_manual">
                <list>
                    <freeCoord comp="x" start="0.0e-3" stop="1.00e-3" inc="0.1e-3"/>
                    <freeCoord comp="z" start="0.0e-3" stop="1.00e-3" inc="0.1e-3"/>
                    <fixedCoord comp="y" value="0.0"/>
                </list>
            </nodes>          
              <nodes name="bc_elec_manual">
                <list>
                    <freeCoord comp="x" start="0.0e-3" stop="1.00e-3" inc="0.1e-3"/>
                    <freeCoord comp="z" start="0.0e-3" stop="1.00e-3" inc="0.1e-3"/>
                    <fixedCoord comp="y" value="2.0e-3"/>
                </list>
            </nodes>    
            <nodes name="saveT">
                <coord x="0.0005" y="0.001" z="0.001"/>
            </nodes>       
        </nodeList>
    </domain>
    
    <sequenceStep index="1">
        <analysis>
            <static></static>
        </analysis>
        <pdeList>
            <heatConduction>
                <regionList>
                    <region name="Vol_EPI"/>
                    <region name="Vol_IMD"/>
                    <region name="Vol_PM"/>
                </regionList>
                <bcsAndLoads>
                    <temperature name="bc_top_manual" value="298.00"/>
                </bcsAndLoads>
                <storeResults>
                    <nodeResult type="heatTemperature">
                        <allRegions/>
                    </nodeResult>
                </storeResults>
            </heatConduction>
        </pdeList>
        
    </sequenceStep>


    <sequenceStep index="2">
        <analysis>
            <transient>               
                <numSteps>
                   3
                </numSteps>
                <deltaT>
                   5E-2
                </deltaT>                
            </transient>            
        </analysis>

        <pdeList>
            <heatConduction  systemId="heat">
                <regionList>
                    <region name="Vol_EPI" nonLinIds="cond cap"/>
                    <region name="Vol_IMD" nonLinIds="cond cap"/>
                    <region name="Vol_PM"/>
                </regionList>

                <nonLinList>
                   <heatConductivity id="cond"/>
                   <heatCapacity id="cap"/>
               </nonLinList>   

               <initialValues>
                  <initialState>
                    <sequenceStep index="1"/>
                 </initialState>
               </initialValues>


                <bcsAndLoads>
                    <temperature name="bc_top_manual" value="298.00"/>

                    <elecPowerDensity name="Vol_IMD">
                      <coupling pdeName="elecConduction">
                          <quantity name="elecPowerDensity"/>
                      </coupling>        
                   </elecPowerDensity>          
                   <elecPowerDensity name="Vol_EPI">
                      <coupling pdeName="elecConduction">
                          <quantity name="elecPowerDensity"/>
                      </coupling>           
                    </elecPowerDensity>          
                </bcsAndLoads>

                <storeResults>
                    <nodeResult type="heatTemperature">
                        <allRegions/>
                        <nodeList>
                          <nodes name="saveT" outputIds="txt"/>
                        </nodeList>
                    </nodeResult>
                </storeResults>
            </heatConduction>
            
            <elecConduction systemId="elecPDE">
                <regionList>
                    <region name="Vol_IMD" matDependIds="cond"/>
                   <region name="Vol_EPI" matDependIds="cond"/>
                </regionList>

                <matDependencyList>
                    <elecConductivity id="cond">
                      <coupling pdeName="heatConduction">
                         <quantity name="heatTemperature"/>
                      </coupling>
                    </elecConductivity>
                </matDependencyList>    

               <bcsAndLoads>
                   <ground name="bc_bottom_manual"/>
                   <potential value="0.5" name="bc_elec_manual"/>         
               </bcsAndLoads>
        
              <storeResults>
                  <nodeResult type="elecPotential">
                     <allRegions/>
                  </nodeResult>
          
                  <elemResult type="elecCurrentDensity">
                   <allRegions/>
                  </elemResult>

                  <elemResult type="elecFieldIntensity">
                   <allRegions/>
                  </elemResult>
          
                  <elemResult type="elecPowerDensity">
                    <allRegions/>
                  </elemResult>
             
                <regionResult type="elecPower">
                   <allRegions outputIds="txt"/>
                </regionResult>
          
              </storeResults>        
           </elecConduction>               
            
        </pdeList>
        

        <couplingList>
            <iterative>
                <convergence logging="yes" maxNumIters="10" stopOnDivergence="no">
                   <quantity name="elecPower" value="1e-3" normType="rel"/>
                </convergence>
            </iterative>
        </couplingList>

    <linearSystems>
      <system id="heat">
        <solutionStrategy>
          <standard>
            <nonLinear logging="yes">
                   <lineSearch/>
                   <incStopCrit> 1e-3</incStopCrit>
                   <resStopCrit> 1e-3</resStopCrit>
                   <maxNumIters> 20  </maxNumIters>
            </nonLinear>
          </standard>
        </solutionStrategy>
        <solverList>
          <pardiso>
          </pardiso>
        </solverList>
      </system>

      <system id="elecPDE">
        <solutionStrategy>
          <standard>
          </standard>
        </solutionStrategy>
        <solverList>
          <pardiso>
          </pardiso>
        </solverList>
      </system>
    </linearSystems>

    </sequenceStep>
    

</cfsSimulation>
