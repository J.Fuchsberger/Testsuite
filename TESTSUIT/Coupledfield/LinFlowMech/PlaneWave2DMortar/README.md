PlaneWave2DMortar
=====================
- This test case solves 2D wave propagation described in [PlaneWave2D](https://gitlab.com/openCFS/Testsuite/-/tree/master/TESTSUIT/Coupledfield/LinFlowMech/PlaneWave2D)
 using the Mortar method.
 
- COMSOL results and geometry files are provided in the above mentioned test case.

