<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
 xsi:schemaLocation="http://www.cfs++.org/simulation ../../../../../share/xml/CFS-Simulation/CFS.xsd"
 xmlns="http://www.cfs++.org/simulation">
  <documentation>
    <title>CylindricalWave2DMortar</title>
    <authors>
      <author>Ferdinand Mond</author>
      <author>Felix Strasser</author>
    </authors>
    <date>2023-02-19</date>
    <keywords>
      <keyword>mechanic-acoustic</keyword>
      <keyword>abc</keyword>
    </keywords>
    <references></references>
    <isVerified>yes</isVerified>
    <description>
      This is the CylindricalWave2D example, modified to use Mortar coupling instead of Nitsche
      2D plane wave propagation with mechanic pressure excitation
      MechanicsLinFlow Mortar coupling is tested in case of bulk viscosity
      The results are symmetric
      The results are compared to the Nitsche case and COMSOL in python script
    </description>
  </documentation>    
  <fileFormats>
    <input>
   <!-- <cdb fileName="circles.cdb"/>-->
      <hdf5 fileName="CylindricalWave2DMortar.h5ref"/>
    </input>
    <output>
      <hdf5/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="plane">
    <variableList>
      <var name="frc" value="1"/>
    </variableList>
    <regionList>
      <region name="S_1" material="solid"></region>
      <region name="S_2" material="FluidMat"></region>
    </regionList>
    <surfRegionList>
      <surfRegion name="ABC1"/>
      <surfRegion name="ABC2"/>
    </surfRegionList> 
    <ncInterfaceList> <!--Note: Mechanics side must be master side >-->
      <ncInterface name="ABC_NC" masterSide="ABC1" slaveSide="ABC2"></ncInterface>
    </ncInterfaceList>        
  </domain>

  <fePolynomialList>
    <Lagrange id="velPolyId">
      <isoOrder>2</isoOrder>
    </Lagrange>
    
    <Lagrange id="presPolyId">
      <isoOrder>1</isoOrder> 
    </Lagrange>
  </fePolynomialList>
  <sequenceStep>
    <analysis>
      <harmonic>
        <numFreq>1</numFreq>
        <frequencyList>
          <freq value="10e3"/>
        </frequencyList>
      </harmonic>
    </analysis>

    <pdeList> 
      <mechanic subType="planeStrain">
        <regionList>
          <region name="S_1" polyId="velPolyId"/>
        </regionList>
        <bcsAndLoads>
          <pressure name="L_c0" value="1"></pressure>
          <fix name="L_x1">
            <comp dof="y"/>
          </fix>
          <fix name="L_y1">
            <comp dof="x"/>
          </fix>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="mechDisplacement">
            <regionList>
              <region name="S_1"/>
            </regionList>
          </nodeResult>
          <nodeResult type="mechVelocity">
            <regionList>
              <region name="S_1"/>
            </regionList>
          </nodeResult>
          <elemResult type="mechStress">
            <regionList>
              <region name="S_1"/>
            </regionList>
          </elemResult>
        </storeResults>
      </mechanic>
      
      <fluidMechLin formulation="compressible" velPolyId="velPolyId" presPolyId="presPolyId">
        <regionList>
          <region name="S_2"/>
        </regionList>
        <bcsAndLoads>
          <noSlip name="L_x2">
            <comp dof="y"/>
          </noSlip>
          <noSlip name="L_y2">
            <comp dof="x"/>
          </noSlip>   
        </bcsAndLoads>
            
        <storeResults>
          <elemResult type="fluidMechStress">
            <allRegions/>
          </elemResult>
          <nodeResult type="fluidMechPressure">
            <regionList>
              <region name="S_2"/>
            </regionList>
          </nodeResult>
          <nodeResult type="fluidMechVelocity">
            <regionList>
              <region name="S_2"/>
            </regionList>
          </nodeResult>
        </storeResults>
      </fluidMechLin>    
    </pdeList>
    
    <couplingList>
      <direct>
        <linFlowMechDirect>
          <ncInterfaceList>
            <ncInterface name="ABC_NC" formulation="Mortar"/>
          </ncInterfaceList>                   
        </linFlowMechDirect>
      </direct>
    </couplingList>

  </sequenceStep>
</cfsSimulation>
