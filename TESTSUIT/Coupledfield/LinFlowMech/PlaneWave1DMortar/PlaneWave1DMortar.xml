<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://www.cfs++.org/simulation ../../../../../share/xml/CFS-Simulation/CFS.xsd"
 xmlns="http://www.cfs++.org/simulation">
 
  <documentation>
    <title> rectangular domain with cylindrical source and mortar interface coupling to outer domain</title>
    <authors>
      <author>Ferdinand Mond</author>
      <author>Felix Strasser</author>
    </authors>
    <date>2023-02-19</date>
    <keywords>
      <keyword>acoustic</keyword>
      <keyword>intersection operations</keyword>
    </keywords>
    <references>
       no reflection at interface
    </references>
    <isVerified>yes</isVerified>
    <description>
      This is the PlaneWave2D example, modified to use Mortar coupling
      Simple channel with mechanic displacement as an excitation on the left side
      Both bulk and shear(dynamic) viscosities are in flow field considered 
      The results are compared to the analytical solution in python script
    </description>
  </documentation>   
  <fileFormats>
    <input>
      <hdf5 fileName="PlaneWave1DMortar.h5ref"/>
<!--  <cdb fileName="recsurf_coupled.cdb"/>-->
    </input>
    <output>
      <hdf5/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  <domain geometryType="plane">
    <regionList>
      <region name="S_flow" material="FluidMat"></region>
      <region name="S_solid" material="solid"></region>
    </regionList>
    <surfRegionList>
      <surfRegion name="ABC1"/>
      <surfRegion name="ABC2"/>
    </surfRegionList> 
    <ncInterfaceList>
      <ncInterface name="ABC_NC" masterSide="ABC1" slaveSide="ABC2"></ncInterface>
    </ncInterfaceList>
  </domain>
  <fePolynomialList>
    <Lagrange  spectral="false" id="velPolyId">
      <isoOrder>2</isoOrder>
    </Lagrange>
    <Lagrange  spectral="false" id="presPolyId">
      <isoOrder>1</isoOrder> 
    </Lagrange>
  </fePolynomialList>

  <sequenceStep>
    <analysis>
      <harmonic>
        <numFreq>1</numFreq>
        <frequencyList>
          <freq value="10000"/>
        </frequencyList>
      </harmonic>
    </analysis>
    
    <pdeList>
      <mechanic subType="planeStrain">
        <regionList>
          <region name="S_solid" polyId="velPolyId" />
        </regionList>
        <bcsAndLoads>
          <displacement name="L_left_solid">
            <comp dof="x" value="1"/>
          </displacement>
          <fix name="L_bottom_solid">
            <comp dof="y"/>
          </fix>
          <fix name="L_top_solid">
            <comp dof="y"/>
          </fix>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="mechVelocity">
            <regionList>
              <region name="S_solid"/>
            </regionList>
          </nodeResult>
          <nodeResult  type="mechDisplacement">
            <regionList>
              <region name="S_solid"/>
            </regionList>
          </nodeResult>
          <elemResult type="mechStress">
            <regionList>
              <region name="S_solid"/>
            </regionList>
          </elemResult>
        </storeResults>
      </mechanic>
        
      <fluidMechLin formulation="compressible" presPolyId="presPolyId" velPolyId="velPolyId">
        <regionList>
          <region name="S_flow"/>
        </regionList>
        <bcsAndLoads>
          <noSlip name="L_bottom_flow">
            <comp dof="y"/>
          </noSlip>
          <noSlip name="L_top_flow">
            <comp dof="y"/>
          </noSlip>
          <velocity name="L_right_flow">
            <comp dof="x" value="0"/>
          </velocity>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="fluidMechPressure">
            <regionList>
              <region name="S_flow"/>
            </regionList>
          </nodeResult>
          <nodeResult type="fluidMechVelocity">
            <regionList>
              <region name="S_flow"/>
            </regionList>
          </nodeResult>
        </storeResults>
      </fluidMechLin>
    </pdeList>

    <couplingList>
      <direct>
        <linFlowMechDirect>
          <ncInterfaceList > 
            <ncInterface name="ABC_NC" formulation="Mortar"/>
          </ncInterfaceList>
        </linFlowMechDirect>
      </direct>
    </couplingList>

  </sequenceStep>
</cfsSimulation>
