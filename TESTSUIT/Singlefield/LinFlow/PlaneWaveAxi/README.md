﻿## Analysis of the axi-symmetric formulation for the LinFlowPDE

###  Description

Axi-symmetric test case where a wave is excited with a 1-x^2 velocity profile that is damped out quickly due to very high artificial viscosity. Simple test case to check axi-symmetric implementation of the LinFlowPDE. For reference the 3D as well as the standard pane version have been computed and can be used for comparison (2D plane just to check that the solution is indeed different).

