<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation">
  <documentation>
    <title>Heat_mech_3d</title>
    <authors>
      <author>Bich Ngoc Vu</author>
    </authors>
    <date>2020-06-11</date>
    <keywords>
      <keyword>thermo-mechanic</keyword>
      <keyword>optimization</keyword>
    </keywords>
    <references>
      none
    </references>
    <isVerified>yes</isVerified>
    <description>
      Two-scale optimization test with multisequence and multi-objective (mech. + therm. compliance) and two material catalogues.
      We have block 'inner' with temperature = 0, which is surrounded by the heated
      design domain. This block excercises a force and
      shall be supported by the design.
    </description>
  </documentation>


  <fileFormats>
    <output>
      <hdf5 />
    </output>
    <materialData file="mat.xml" />
  </fileFormats>

  <domain geometryType="3d">
    <regionList>
      <region name="mech" material="99lines" />
      <region name="inner" material="99lines" />
    </regionList>
    <nodeList>
      <nodes name="source">
        <allNodesInRegion regName="mech" />
      </nodes>
      <nodes name="inner_nodes">
        <allNodesInRegion regName="inner" />
      </nodes>
    </nodeList>
  </domain>

  <sequenceStep index="1">
    <analysis>
      <static />
    </analysis>
    <pdeList>
      <mechanic subType="3d">
        <regionList>
          <region name="mech" />
          <region name="inner" />
        </regionList>

        <bcsAndLoads>
          <force name="inner_nodes">
            <comp dof="y" value="-1" />
          </force>
          <fix name="bottom">
            <comp dof="x" />
            <comp dof="y" />
          </fix>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions />
          </nodeResult>
          <elemResult type="mechTensor">
            <allRegions />
          </elemResult>
          <elemResult type="optResult_1">
            <allRegions />
          </elemResult>
          <elemResult type="optResult_2">
            <allRegions />
          </elemResult>
          <elemResult type="optResult_3">
            <allRegions />
          </elemResult>
          <elemResult type="optResult_4">
            <allRegions />
          </elemResult>
          <elemResult type="optResult_5">
            <allRegions />
          </elemResult>
          <elemResult type="optResult_6">
            <allRegions />
          </elemResult>
          <elemResult type="optResult_7">
            <allRegions />
          </elemResult>
          <elemResult type="optResult_8">
            <allRegions />
          </elemResult>
          <elemResult type="optResult_9">
            <allRegions />
          </elemResult>
          <elemResult type="optResult_10">
            <allRegions />
          </elemResult>
        </storeResults>
      </mechanic>
    </pdeList>

    <linearSystems>
      <system>
        <solutionStrategy>
          <standard />
        </solutionStrategy>
        <solverList>
          <pardiso />
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>

  <sequenceStep index="2">
    <analysis>
      <static />
    </analysis>
    <pdeList>
      <heatConduction>
        <regionList>
          <region name="mech" />
          <region name="inner" />
        </regionList>

        <bcsAndLoads>
          <heatSource name="source" value="10" />
          <temperature name="inner_nodes" value="0" />
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="heatTemperature">
            <allRegions />
          </nodeResult>
          <elemResult type="heatConductivityTensor">
            <allRegions/>
          </elemResult>
        </storeResults>
      </heatConduction>
    </pdeList>

    <linearSystems>
      <system>
        <solutionStrategy>
          <standard />
        </solutionStrategy>
        <solverList>
          <pardiso />
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>

  <optimization log="">
    <costFunction type="multiObjective" task="minimize" sequence="1" region="all" linear="false">
      <multiObjective>
        <objective penalty="0.8" type="compliance" sequence="1" />
        <objective penalty="0.1" type="heatEnergy" sequence="2" />
      </multiObjective>
      <stopping queue="10" value="0.001" type="designChange" />
    </costFunction>
    <constraint type="globalTwoScaleVolume" design="allDesigns" bound="upperBound" value="0.3" linear="false" access="filtered">
      <local normalize="true" power="1" />
    </constraint>

    <optimizer type="snopt" maxIterations="3">
      <snopt>
        <option name="major_optimality_tolerance" type="real" value="1e-6" />
        <option name="verify_level" type="integer" value="-1" />
      </snopt>
    </optimizer>

    <ersatzMaterial region="mech" material="mechanic" method="paramMat">
      <materials>
        <material type="mechanic" sequence="1" />
        <material type="heat" sequence="2" />
      </materials>
      <paramMat>
        <designMaterials>
          <designMaterial type="hom-rect-C1" sequence="1">
            <param name="rotAngleFirst" value="0." />
            <param name="rotAngleSecond" value="0." />
            <param name="rotAngleThird" value="0." />
            <homRectC1 file="catalogue_mech.xml" interpolation="c1" />
          </designMaterial>
          <designMaterial type="heat" sequence="2">
            <heat file="catalogue_heat.xml" interpolation="c1" />
          </designMaterial>
        </designMaterials>
      </paramMat>
      <filters>
        <filter type="density" design="stiff1" neighborhood="radius" value="0.2" />
        <filter type="density" design="stiff2" neighborhood="radius" value="0.2" />
        <filter type="density" design="stiff3" neighborhood="radius" value="0.2" />
      </filters>

      <design name="stiff1" region="mech" initial="0.2" lower="1e-9" upper="1.0" />
      <design name="stiff2" region="mech" initial="0.2" lower="1e-9" upper="1.0" />
      <design name="stiff3" region="mech" initial="0.2" lower="1e-9" upper="1.0" />
      <result value="design" design="stiff1" id="optResult_1" access="smart" />
      <result value="design" design="stiff2" id="optResult_2" access="smart" />
      <result value="design" design="stiff3" id="optResult_3" access="smart" />
      <result value="design" design="stiff1" id="optResult_4" access="plain" />
      <result value="design" design="stiff2" id="optResult_5" access="plain" />
      <result value="design" design="stiff3" id="optResult_6" access="plain" />
      <result value="homogenizedVolume" design="allDesigns" id="optResult_7" access="smart" />
      <result value="design" design="mech_11" id="optResult_8" access="smart" />
      <result value="design" design="mech_22" id="optResult_9" access="smart" />
      <result value="design" design="mech_33" id="optResult_10" access="smart" />
      <export write="iteration" save="last" />
    </ersatzMaterial>
    <commit mode="forward" stride="9999" />
  </optimization>


</cfsSimulation>
